import { createStore , combineReducers , applyMiddleware} from "redux";
import businesses from '../reducers/businesses';
import errors from '../reducers/errors';
import thunk from 'redux-thunk';

const store = createStore(
    combineReducers({businesses, errors}), applyMiddleware(thunk)
    )

export default store