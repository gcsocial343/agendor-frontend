import Axios from 'axios';
import UUID from 'uuid/v4';

const API_URL = process.env.API_URL || 'http://localhost:3000/'

export const changeCardStage = (businessId, stageId) => dispatch => {
    Axios.put(API_URL + 'businesses/' + businessId, { stage: stageId })
        .then(_ => dispatch({
            type: 'CHANGE_CARD_STAGE',
            businessId,
            stageId
        }))
        .catch(() => { 
            const errorId = UUID()
            dispatch({
                type: 'ADD_ERROR',
                error: {
                    id: errorId,
                    message: 'Não foi possível alterar o estágio do seu negócio'
                }
            })
            setTimeout( () => {
                dispatch({ 
                    type: 'REMOVE_ERROR',
                    error: errorId
                })
            } , 5000 ) 
        })
}

export const fetchCards = () => dispatch => {
    Axios.get( API_URL + 'businesses')
        .then(response => dispatch({
            type: 'FETCH_CARDS',
            businesses: response.data
        }))
        .catch(() => { 
            const errorId = UUID()
            dispatch({
                type: 'ADD_ERROR',
                error: {
                    id: errorId,
                    message: 'Não foi possível carregar todos os seus negócios'
                }
            })
            setTimeout( () => {
                dispatch({ 
                    type: 'REMOVE_ERROR',
                    error: errorId
                })
            } , 5000 )
        })
}

export const saveCard = (businessId, business) => dispatch => {
    return Axios.post( API_URL + 'businesses', business)
        .then(response =>
            dispatch({
                type: 'SAVE_CARD',
                businessId,
                business: response.data
            }))
        .catch(() => { 
            const errorId = UUID()
            dispatch({
                type: 'ADD_ERROR',
                error: {
                    id: errorId,
                    message: 'Não foi possível salvar'
                }
            })
            setTimeout( () => {
                dispatch({ 
                    type: 'REMOVE_ERROR',
                    error: errorId
                })
            } , 5000 )
        })
}

export const cancelCard = (businessId) => ({
    type: 'CANCEL_CARD',
    businessId
})

export const newCard = () => ({
    type: 'NEW_CARD'
})

export const updateCard = (businessId, field, value) => ({
    type: 'UPDATE_CARD',
    businessId,
    field,
    value
})