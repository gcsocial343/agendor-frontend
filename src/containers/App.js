import {connect} from 'react-redux';


import App from '../components/App';
import { newCard , fetchCards } from '../actions';

const mapDispatchToProps = dispatch => ({
    newCard: () => dispatch(newCard()),
    fetchCards: () => dispatch(fetchCards())
})

export default connect(null,mapDispatchToProps)(App)