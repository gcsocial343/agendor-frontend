import { connect } from 'react-redux';
import Stage from '../components/Stage';

import { changeCardStage, saveCard, cancelCard, updateCard } from '../actions'

const getbusinessesOfStage = (state, stageId) => state.businesses.filter(business => business.stage === stageId)

const mapStateToProps = (state, props) => ({
    items: getbusinessesOfStage(state, props.id),
    id: props.id,
    title: props.title,
    type: props.type,
})

const mapDispatchToProps = dispatch => ({
    changeCardStage: (id, stage) => dispatch(changeCardStage(id, stage)),
    saveCard: (id, business) => dispatch(saveCard(id, business)),
    cancelCard: (id) => dispatch(cancelCard(id)),
    updateCard: (id,field,value) => dispatch(updateCard(id,field,value))
})

export default connect(mapStateToProps, mapDispatchToProps)(Stage);