import Error from '../components/Error';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    errors: state.errors
})


export default connect(mapStateToProps)(Error)