const INITIAL_STATE = []

const businesses = ( state  = INITIAL_STATE, action ) => {
    

    switch(action.type){
        case 'FETCH_CARDS':
            return  [ ...action.businesses]
        case 'CANCEL_CARD':
            return state.filter( business => business.id !== action.businessId );
        case 'NEW_CARD':
            const newId = state.length + 1
            const newCard= { id: newId , stage: "1" , company: '', name: '', value: null , newCard: true }
            return [ newCard, ...state  ]
        default:
            return state.map( o => business(o,action))
    }
}

const business = ( state , action ) => {
    if( action.businessId !== state.id ){
        return state
    }
    switch(action.type){
        case 'CHANGE_CARD_STAGE':
            state.stage = action.stageId
            return state
        case 'SAVE_CARD':
            return action.business
        case 'UPDATE_CARD':
            return {...state ,[action.field]: action.value}
        default:
            return state
    }
}
export default businesses