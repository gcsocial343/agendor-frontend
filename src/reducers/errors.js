const errors = ( state = [] , action ) => {
    switch( action.type ){
        case 'ADD_ERROR':
            return [ ...state , action.error ]
        case 'REMOVE_ERROR':
            return state.filter( error => error.id !== action.error )
        default: 
            return state
    }
}
export default errors