import React, { Component } from 'react';
import Stage from '../containers/Stage';
import Error from '../containers/Error';
import './App.css';

class App extends Component {
  componentDidMount(){
    this.props.fetchCards()
  }
  render() {
    const { newCard } = this.props
    return (
      <div className="container">
        <Error />
        <div className="row">
          <div className="button" onClick={ newCard }>
            Adicionar negócio
          </div>
        </div>
        <div className="row stages">
          <Stage id={1} title="Contato" type="default" />
          <Stage id={2} title="Envio de Proposta" type="default" />
          <Stage id={3} title="Follow-up" type="default" />
          <Stage id={4} title="Fechamento" type="default" />
          <Stage id={5} title="Ganhos" type="earns" />
          <Stage id={6} title="Perdidos" type="losses" />
          </div>
      </div>
    );
  }
}

export default App;
