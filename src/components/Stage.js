import React , { Component } from 'react';
import classNames from 'classnames';
import Card from './Card';
import Numeral from 'numeral';
import './Stage.css';

class Stage extends Component {
    totalAmountOfItems(){
        const amount = this.props.items.length;
        const suffix = amount > 1 ? "negócios" : "negócio"
        return amount + " " + suffix
    }
    totalValueOfItems(){
        const value = this.props.items.reduce( (prev,current) => {
           return prev + parseFloat(current.value)
        },0)
        return  Numeral(value).format('$0,0.00')
    }
    updatebusinessToThisStage(e){
        const { changeCardStage , id } = this.props
        const businessId = parseInt(e.dataTransfer.getData('id'))
        changeCardStage(businessId ,id)
    }
    handleDragOver(e){
       return  e.preventDefault()
    }
    render(){
        const { title , items, type } = this.props;
        return(<div className="column" onDragOver={this.handleDragOver } onDrop={ e => this.updatebusinessToThisStage(e) }>
            <div className="column-warper">
            <div className={classNames(["column-header",{ default: type === "default"  },{ earns: type === "earns"  },{ losses: type === "losses"  }])}>
                <div className="column-title">
                    { title }
                </div>
                <div className="column-body">
                    <span>
                        { this.totalValueOfItems() }
                    </span>
                    <span>
                        {this.totalAmountOfItems() }
                    </span>
                </div>
            </div>
            { items.map(  (item) => 
            
                    <Card key={item.stage + ' ' + item.id } 
                        item={item} 
                        cancelCard={this.props.cancelCard}  
                        saveCard={this.props.saveCard}
                        updateCard={this.props.updateCard}
                    />
                    ) }
                    </div>
        </div>)
    }
}

Numeral.register('locale', 'pt-BR', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    currency: {
        symbol: 'R$ '
    }
});

Numeral.locale('pt-BR');

export default Stage