import React , { Component } from 'react';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import Numeral from 'numeral';

import './Card.css';

class Card extends Component {
    constructor(props){
        super(props)

        this.state = props.item
    }   
    handleNewCardFieldChange(e,field){
        const {item, updateCard }=this.props
        updateCard(item.id ,field , e.target.value )
    }
    handleCardValueChange(e){
        const {item, updateCard }=this.props
        const masked = e.target.value
        const raw = parseInt(masked.replace(/(R\$.|\.|_)/g,'').replace(/,/g,'.')) || 0
        if( typeof raw === "number"){
            updateCard(item.id ,'value' , raw )
        }
    }
    render(){
        const { id , company , value , name, newCard  } = this.props.item
        return(<div className="card" draggable={!newCard} onDragStart={ e => {
            e.dataTransfer.setData('id', id  )}}>
            <div className="card-business">
                <Field 
                    newCard={newCard}
                    value={name}
                    placeholder="Nome do negócio"
                    onChange={ e => this.handleNewCardFieldChange(e,'name')} 
                />
                { newCard && <span className="actions">
                    <div onClick={ () => this.props.saveCard(id,this.props.item)}>
                        <img src="/img/check.png" alt="" />
                    </div>
                    <div className="remove" onClick={ () => this.props.cancelCard(id)}>
                        <img src="/img/remove.png" alt=""  />
                    </div>
                </span> }
            </div>
            <div className="card-company">
                <img  src="/img/company.png" alt=""/>
                <Field 
                    newCard={newCard}
                    value={company}
                    placeholder="Empresa"
                    onChange={ e => this.handleNewCardFieldChange(e,'company')} 
                />
                
            </div>
            <div className="card-value">
                <CurrencyField 
                    newCard={newCard} 
                    value={value} 
                    placeholder= "R$ 0.000,00"
                    onChange={ e => this.handleCardValueChange(e) } 
                />
            </div>
        </div>)
    }
}

const Field = ({ value, newCard, onChange, placeholder }) =>{
    if(newCard){
       return <input onChange={onChange} value={value} placeholder={placeholder} /> 
    } else {
        return value
    }
}

const CurrencyField = ({ value, newCard , onChange , placeholder }) => {
    if(newCard){
        return <MaskedInput 
        value={value} 
        onChange={onChange} 
        placeholder={placeholder}
        mask={createNumberMask({ prefix: 'R$ ',thousandsSeparatorSymbol:".", modelClean: true,
        requireDecimal: true,
        decimalSymbol: ","})}  
        
        />
    } else {
        return  Numeral(parseFloat(value)).format('$0,0.00') 
    }
}

export default Card