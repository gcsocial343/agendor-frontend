import React , { Component } from 'react';
import './Error.css'
class Error extends Component {
    render(){
        const { errors } = this.props
        return(
            <div className='errors'>
                { errors.map( error => 
                    <div className='error' key={error.id}>
                        { error.message }
                    </div>
                    )}
                
            </div>
        )
    }
}

export default Error